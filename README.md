# Boring numbers


### PL

Rozważmy operację, która dodatniej liczbie całkowitej przyporządkowuje sumę kwadratów jej
cyfr w zapisie dziesiętnym. Przykładowo: liczbie 123 zostanie przyporządkowana liczba 14,
ponieważ 12 + 2
2 + 3
2 = 14. Utwórzmy teraz ciąg, którego pierwszym elementem będzie
dodatnia liczba całkowita n, a każdy kolejny jego element to wynik zastosowania powyższej
operacji do elementu poprzedzającego go w tym ciągu.
Jeśli w otrzymanym w ten sposób ciągu pojawi się liczba 1, to początkową liczbę n nazywamy
liczbą nudną, w przeciwnym razie n nazywamy liczbą ciekawą.

### ENG

Consider an operation that assigns to a positive integer the sum of the squares of its
digits in decimal notation. For example: the number 123 will be assigned the number 14,
because 12 + 2
2 + 3
2 = 14. Now let's create a sequence, the first element of which will be a
positive integer n, and each of its subsequent elements is the result of applying the above
operation to the element preceding it in this sequence.
If the number 1 appears in the sequence thus obtained, then the initial number n is called the
a boring number, otherwise n is called an interesting number.