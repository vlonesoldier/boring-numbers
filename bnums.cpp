#include <iostream>
#include <string>
#include <map>

using namespace std;

bool arr[1000];

// n < 1000
// calculated sum will never be over 243
int sumSquareOfNums(int n)
{
    int sum = 0;

    while (n > 0)
    {
        int num = (n % 10) * (n % 10);
        sum += num;
        n /= 10;
    }
    return sum;
}

string isBoringNum(int n)
{
    int num = sumSquareOfNums(n);

    if (num == 1)
        return "true";
    else if (arr[num])
        return "false";

    arr[num] = true;
    return isBoringNum(num);
}

int main()
{

    for (int i = 0; i < 1000; i++)
        arr[i] = false;

    cout << "Is given number a \"boring number\"? " << isBoringNum(13);
    return 0;
}